# Welcome

Welcome to this web space featuring various sample projects utilizing AWS cloud computing services. The projects primarily cover the following software engineering domains:

* Creation of cloud computing infrastructure resources and implementation of tagging best practices using Infrastructure as Code (IaC) tools such as [Terraform](https://www.terraform.io) and [AWS CloudFormation](https://aws.amazon.com/cloudformation/).

* A practical solution for redacting Personal Identifiable Information (PII) using [Amazon Comprehend](https://aws.amazon.com/comprehend/) and [AWS Serverless computing](https://aws.amazon.com/serverless/) technologies. Amazon Comprehend is a natural language processing (NLP) service that utilizes machine learning (ML) to identify useful insights and correlations in text data. AWS Serverless Computing 
offers a range of technologies for code execution, data management, and application integration, 
without the need to manage servers. Serverless technologies enable automatic scaling, 
built-in high availability, and a pay-as-you-go billing model, which enhances agility and cost optimization.
